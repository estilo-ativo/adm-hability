import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Subject } from 'rxjs';
import { GroupAdmsService } from '../group-adms.service';
import { Adm, UserDetails } from '../group-adms.type';

@Component({
  selector: 'app-register-group-adms',
  templateUrl: './register-group-adms.component.html',
  styleUrls: ['./register-group-adms.component.scss']
})
export class RegisterGroupAdmsComponent implements OnInit, OnChanges {

  newAdm: Adm = {
    name: '',
    listUserBackoffice: [],
    idAdm: '',
    active: true
  };

  newUserBackoffice: UserDetails = {
    username: '',
    email: '',
    firstName: '',
    password: '',
    active: true,
    profile: 'administrador',
    others: {},
    groupAdm: ''
  }

  private _unsubscribeAll: Subject<any>;

  constructor(
    private _groupAdmsService: GroupAdmsService
  ) {
    this._unsubscribeAll = new Subject();

  }

  ngOnChanges(changes: SimpleChanges): void {
  }

  register() {
    this.newUserBackoffice.groupAdm = this.newAdm.idAdm;

    this._groupAdmsService.createGroupAdm(this.newAdm).subscribe(resp => {
      console.log(resp);
      this._groupAdmsService.createUserBackoffice(this.newUserBackoffice).subscribe(resp => {
        location.reload();
      });
    });

    console.log(this.newAdm);
    console.log(this.newUserBackoffice);
  }

  ngOnInit(): void {

  }

  ngOnDestroy(): void
  {
      // Unsubscribe from all subscriptions
      this._unsubscribeAll.next();
      this._unsubscribeAll.complete();
  }

}
