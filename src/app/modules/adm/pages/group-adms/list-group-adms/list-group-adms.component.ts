import { Component, OnInit, ViewEncapsulation, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { GroupAdmsService } from '../group-adms.service';
import { Adm } from '../group-adms.type';

@Component({
  selector: 'list-group-adms',
  templateUrl: './list-group-adms.component.html',
  styleUrls: ['./list-group-adms.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ListGroupAdmsComponent implements OnInit, OnDestroy {

  // Private
  private _unsubscribeAll: Subject<any>;

  listAdms: Adm[] = [];
  selectedAdm: Adm | null = null;

  constructor(
    private _groupAdmsService: GroupAdmsService
  ) {
      // Set the private defaults
      this._unsubscribeAll = new Subject();
  }

  ngOnInit(): void {

    //Get List Adms
    this._groupAdmsService.getAllAdms().pipe(takeUntil(this._unsubscribeAll)).subscribe((listAdms) => {
      console.log('Lista de Adms', listAdms);
      this.listAdms = listAdms;
    });
  }

  activeDisableAdm(adm: Adm) {
    adm.active = adm.active ? false: true;
    this._groupAdmsService.updateGroupAdm(adm).subscribe(resp => {
      console.log('Atualização: ', resp);
    });
  }

  groupSelected(adm: Adm) {
    this.selectedAdm = adm;
  }

  ngOnDestroy(): void
  {
      // Unsubscribe from all subscriptions
      this._unsubscribeAll.next();
      this._unsubscribeAll.complete();
  }

}
