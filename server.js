var express = require('express');
var app = express();

app.all('*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
  });

app.use(express.static(__dirname + '/dist/adm-hability'));
// app.get('/*', function (req, res,next) {
//     res.sendFile('/dist/fed-habiliy/index.html');
// });

app.use("*", function(req, resp) {
    resp.sendFile(__dirname + '/dist/adm-hability/index.html');
  });

app.listen(8080)
